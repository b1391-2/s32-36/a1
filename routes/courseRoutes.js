
const express = require("express");
const router = express.Router();

const courseController = require("./../controllers/courseControllers")
const auth = require("./../auth");

//create a course
router.post("/create-course", auth.verify, (req, res) => {
	courseController.createCourse(req.body).then(result => res.send(result))
})


//retrieving all courses
router.get("/",(req, res) => {
	courseController.getAllCourses().then(result => res.send(result))
})


//retrieving only active courses
router.get("/active-courses", auth.verify, (req, res) => {
	courseController.getActiveCourses().then(result => res.send(result))
})

//get a specific course using findOne()
router.get("/specific-course", auth.verify, (req,res) => {
	//console.log(req.body);
	courseController.getSpecificCourse(req.body.courseName).then(result => res.send(result));
})

//get specific course using findbyID()
router.get("/:courseId", auth.verify, (request, response) =>{
	//console.log(request.params); //{ courseId: '619b698bacd17db2b185d07b' }
	let paramsId = request.params.courseId;
	courseController.getCourseById(paramsId).then(result => response.send(result));
})


//update isActive status of the course using findOneAndUpdate()
	//update isActive status to false
router.put("/archive", auth.verify, (request, response) => {
	console.log(request.body.courseName)
	courseController.archiveCourse(request.body.courseName).then(result => response.send(result));
})

	//update isActive status to true
router.put("/unarchive", auth.verify, (request, response) => {
	console.log(request.body.courseName)
	courseController.unarchiveCourse(request.body.courseName).then(result => response.send(result));
})
//update isActive status of the course using findByIdAndUpdate()

//update isActive status to false

router.put("/:courseId/archive", auth.verify,(request, response) => {
	console.log(request.params.courseId);
	courseController.archiveCourseById(request.params.courseId).then(result => response.send(result));
})


//update isActive status to true
	
router.put("/:courseId/unarchive", auth.verify ,(request, response) => {
	console.log(request.params.courseId);
	courseController.unarchiveCourseById(request.params.courseId).then(result => response.send(result));
})



//delete course using findOneAndDelete()
router.delete("/delete-course", auth.verify ,(request, response) => {
	console.log(request.body)
	courseController.deleteCourse(request.body.courseName).then(result => response.send(result))
})


//delete course using findByIdandDelete()

router.delete("/:courseId/delete-course", auth.verify,(request, response) => {
	console.log(request);
	console.log(request.params.courseId);
	courseController.deleteCourseById(request.params.courseId).then(result => response.send(result));
})

//Update course using the ID of the course


router.put("/:courseId/update-course", auth.verify, (request, response) => {
	//console.log(request.params.courseId);
	//console.log(request.body);
	courseController.updateCourse(request.params.courseId, request.body).then(result => response.send(result));
})

module.exports = router;