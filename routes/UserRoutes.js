const express = require("express");
const router = express.Router();

const userController = require("./../controllers/UserControllers");
const auth = require("./../auth");

//check if email exist
router.get("/email-exists", (request, response) => {
	userController.checkEmail(request.body).then((result) => response.send(result));
})

//register a user
// http://localhost:4000/api/users
router.post("/register", (request, response) => {
	
	let userItems = request.body;

	//console.log(userItems);

	userController.register(userItems).then(result => response.send(result));
})


//retrieve all user
router.get("/", (request, response) => {
	userController.getAllUsers().then((result, error) => response.send(result));
})

//Log in
router.post("/login", (request, response) => {
	console.log(request.body);
	userController.logIn(request.body).then((result) => response.send(result));
})

//Retrieve user information

router.get("/details", auth.verify ,(request, response) => {
	//id
	//email
	//isAdmin
	let userData = auth.decode(request.headers.authorization);
	//console.log(userData);

	/*
	Value of the token

	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYmUzYmZhNjAxZWQ1MDkxODg0MjZlYSIsImVtYWlsIjoiY2FsdmluQHlhaG9vLmNvbSIsImlzQWRtaW4iOnRydWUsImlhdCI6MTYwNjc0MDU4N30.8tH4wJ2gikHOHxC6U4tFir4K6ZRLqSEuEMIgtn0FguY
	*/

	userController.getProfile(userData).then(result => response.send(result));


})

// Enroll the user
router.post("/enroll", auth.verify, (request, response) => {
	//console.log(request.headers.authorization.id);

	let data = {
		userId : auth.decode(request.headers.authorization).id,
		courseId : request.body.courseId
	}

	console.log(data);
	

	userController.enrollCourse(data).then(result => response.send(result));
})



module.exports = router;
