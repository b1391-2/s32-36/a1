/* To create JSON Webtoken */


const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//to create a token
	//sign(first parameter data of the user, secret key, options which is an object {}) 
	//verify
	//decode

	return jwt.sign(data, secret, {});
	

}


module.exports.verify = (request, response, next) => {
	//get the token in the headers authorization

	
	//console.log(token);
	//console.log(typeof token);
	let token = request.headers.authorization;
	
	//console.log(token);
	if(typeof token != "undefined"){
		
		token = token.slice(7, token.length)
		//jwt verify(token, secret, cbfunction (error, data) )
		return jwt.verify(token, secret, (error, data) => {
			//console.log(data);
			if (error) {
				return response.send({auth: "failed"})
			} else {
				next()
			}
		})
	} 
}

module.exports.decode = (token) => {
	//console.log(token)
	token = token.slice(7, token.length);
	//console.log(NewToken);

	if (typeof token != "undefined") {

		//console.log((jwt.decode(token, {complete: true})));
		//console.log((jwt.decode(token, {complete: true})).payload);

		return jwt.verify(token, secret, (error, data) => {
			if (error) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})

	}
}