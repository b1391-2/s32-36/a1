//Importing User Model

const User = require("./../models/User");
const Course = require("./../models/Course");


//Requiring Bcrypt to hash passwords
const bcrypt = require("bcrypt");

//Calling auth
const auth = require("./../auth");

module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody
	return User.findOne({email: email}).then((result, error) => {
		//to check if something is found
		//console.log(result);

		if (result != null) {
			return `Email already exists`
		} else {
			if (result) {
				return true
			} else {
				return error
			}
		}
	})
}

module.exports.register = (reqBody) => {

	//console.log(reqBody);

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		mobileNo : reqBody.mobileNo
	})
	//save()
	return newUser.save().then((result, error) => {

		//console.log(result);
		if (result) {
			return true
		} else {
			return error
		}
	})
}

module.exports.getAllUsers = () => {

	return User.find().then((result, error) => {
		console.log(result);
		if (result){
			return result
		} else {
			return error
		}
	})
}

module.exports.logIn = (reqBody) => {
	console.log(reqBody);

	const {email, password} = reqBody;

	return User.findOne({email : email}).then((result, error) => {
		//Filter found
		//console.log(result);
		//console.log(result.password);
		if (result == null) {
			return false
		} else {
			//What if we found the email but the password is incorrect
			let isPasswordCorrect = bcrypt.compareSync(password, result.password);
			
			if (isPasswordCorrect == true) {
				//console.log(auth.createAccessToken(result));
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	console.log(data);
	const {id, firstName, lastName, mobileNo} = data;

	return User.findOne({id : id}).then((result, error) => {
		// console.log(result);

		result = {
			firstName : result.firstName,
			lastName : result.lastName,
			mobileNo : result.mobileNo,
			password : "",
		}
		return result
	})
}

module.exports.enrollCourse = async (data) => {

	const { userId, courseId} = data

	console.log(data);
	//Look for a matching document of a user
	const userEnroll = await User.findById(userId).then((result, error) => {
		//console.log(result);
		if (error) {
			return error
		} else {
			console.log(result);
			//console.log(result.enrollments);
			//Manipulate the enrollments array by adding the course id to the userid
			result.enrollments.push({courseId : courseId});

			if (result.enrollments.courseId != courseId) {
				//save the changes
				return result.save().then((result, error) => {
					if (error) {
						return error
					} else {
						return true
					}
				})
			} else {
				return false
			}
		}
	})

	//Look for matching document of a course
	const courseEnroll = await Course.findById(courseId).then((result, error) => {
		//console.log(result);
		if (error) {
			return error
		} else {
			//console.log(result);

			result.enrollees.push({userId : userId});

			if (result.enrollees.userId != userId) {
				return result.save().then((result, error) => {
					if (error) {
						return error
					} else {
						return true
					}
				})
			} else {
				return false
			}
		}


		if (userEnroll && courseEnroll){
			return true
		} else {
			return false
		}
	})
}
