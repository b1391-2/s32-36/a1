
const Course = require("./../models/Course");

module.exports.createCourse = (reqBody) => {
	let newCourse = new Course({
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.getAllCourses = () => {

	return Course.find().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}


module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then((result, err) => {
		if(err) {
			return false
		} else {
			return result
		}
	})
}

//get a specific course using findOne()

module.exports.getSpecificCourse = (reqBody) => {
	//console.log(reqBody);

	//Look for a matching document in the database


	return Course.findOne({courseName: reqBody}).then( (result, error) => {
		console.log(result);
	//if matching document found, return a document
	if (result == null){
		return `Course not existing`
	} else {
		//if error, return the error
		if (result){
			return result
		} else {
			return error
		}
	}
	})
}

//get specific course using findById()
module.exports.getCourseById = (params) => {
	//console.log(params);
	//look for matching document
	return Course.findById(params).then((result, error) => {
		// console.log(result);
		// If matching document not found, return course not existing
		if (result == null) {
			return `Course Not Existing`
		} else {
			if (result) {
				//return matching document
				return result; 
			} else {
				//return error
				return error
			}
		}
	})
}


//update isActive status of the course using findOneAndUpdate()
module.exports.archiveCourse = (reqBody) => {
	console.log(reqBody);
	let courseStatus = {
		isActive : false
	}

	return Course.findOneAndUpdate({courseName: reqBody}, courseStatus).then((result, error) => {
		console.log(result);
		if (result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return result
			} else {
				return error
			}
		}
	})
}

module.exports.unarchiveCourse = (reqBody) => {
	console.log(reqBody);
	let courseStatus = {
		isActive : true
	}

	return Course.findOneAndUpdate({courseName: reqBody}, courseStatus).then((result, error) => {
		console.log(result);
		if (result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return result
			} else {
				return error
			}
		}
	})
}
//update isActive status of the course using findByIdandUpdate()

module.exports.archiveCourseById = (params) => {

//update isActive status to false
	return Course.findByIdAndUpdate(params, {isActive: false}).then((result, erorr) => {
		console.log(result)
		if (result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return true
			} else {
				return false
			}
		}
	})
}

	

//update isActive status to true

module.exports.unarchiveCourseById = (params) => {

	
	return Course.findByIdAndUpdate(params, {isActive: true}).then((result, erorr) => {
		console.log(result)
		if (result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return true
			} else {
				return false
			}
		}
	})
}
	

//delete course using findOneAndDelete()

module.exports.deleteCourse = (reqBody) => {
	//look for matching documents & delete the matching document

	return Course.findOneAndDelete({courseName : reqBody}).then((result, error) =>{
		if (result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return result 
			} else {
				return error
			}
		}
	})
	
}


//delete course using findByIdandDelete

module.exports.deleteCourseById = (id) => {
	console.log(id);
	return Course.findByIdAndDelete(id).then((result, error) => {
		if (result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return true 
			} else {
				return error
			}
		}
	})
}

//Update course

module.exports.updateCourse = (id, reqBody) => {
	//console.log(id);
	//console.log(reqBody);

	const { courseName, description, price} = reqBody;
	
	let updatedCourse = {
		courseName : courseName,
		description : description,
		price : price
	}
	return Course.findByIdAndUpdate(id, updatedCourse, {new: true}).then((result, error) => {
		//console.log(result);
		if (error) {
			return error
		} else {
			return result
		}
	})
}